set -e
cp ./target/x86_64-pc-windows-msvc/release/receiver.exe ./receiver.exe
cp ./target/x86_64-pc-windows-msvc/release/sender.exe ./sender.exe

git commit ./receiver.exe ./sender.exe -m "updating executables"
git push