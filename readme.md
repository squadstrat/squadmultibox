sends keys to another machine
builds src/bin/receiver.rs and src/bin/sender.rs, check there for usage
build --release --target=x86_64-pc-windows-msvc

Usage:
The receiver simply listens on a given port, but expects the full `ip:port` address of the host.
To find this you can run `ipconfig` in cmd. It should be listed as `IPv4 Address`.
Then, also on the command line you can run for example: `.\receiver.exe 192.168.100.2:1337`

On the sender side you can run: `.\sender.exe 192.168.100.2:1337`

Hardcoded key map:

*sender.exe => receiver OS*

- W => W
- A => A
- S => S
- D => D
- R => E

planned:

*sender.exe => sender OS*
- Q => A 
- E => D

*(for turret turning)*


