extern crate winapi;

use std::collections::HashMap;
use std::mem::MaybeUninit;
use std::ptr::null_mut;
use std::thread;
use hookmap::prelude::Button;
use winapi::shared::minwindef::{HINSTANCE, LPARAM, LRESULT, WPARAM};
use winapi::shared::windef::{HHOOK__, HWND};
use winapi::um::winuser::{CallNextHookEx, GetActiveWindow, GetMessageW, KBDLLHOOKSTRUCT, MSG, PostMessageW, SetWindowsHookExW, WH_KEYBOARD_LL, WM_KEYDOWN, WM_KEYUP, WM_SYSKEYDOWN, WM_SYSKEYUP};
use crate::keyboard::{KeyEvent, Direction};
use crate::keycodes::KEY_TO_KEYCODE;

//use crate::keycodes::{KEYCODE_NAMES};
pub type Window = HWND;

pub const WINDOWS_FAIL_MULTIPLIER: usize = 2;



#[cfg_attr(feature = "burn", flame)]
pub fn get_window_handle() -> Option<Window> {
  use std::ffi::OsStr;
  use std::iter::once;
  use std::os::windows::ffi::OsStrExt;
  use std::ptr::null_mut;
  use winapi::um::winuser::FindWindowW;
  let class: Vec<u16> = OsStr::new("UnrealWindow").encode_wide().chain(once(0)).collect();
  let title: Vec<u16> = OsStr::new("SquadGame  ").encode_wide().chain(once(0)).collect();
  let hwnd = unsafe { FindWindowW(class.as_ptr(), title.as_ptr()) };
  if hwnd.is_null() {
    None
    //println!("using active window");
    //let active = unsafe {GetActiveWindow()};
    //Some(active)
  } else {
    Some(hwnd)
  }
}

#[cfg_attr(feature = "burn", flame)]
pub fn send_key(hwnd: Window, key_action: KeyEvent) {
  //let key_codes: HashMap = KEYCODES;
  let maybe_code= KEY_TO_KEYCODE.get(&key_action.key);
  if let Some(code) = maybe_code{
    unsafe {
      match key_action.action {
        Direction::Down => PostMessageW(hwnd, WM_KEYDOWN, *code as usize, 0x00000000),
        Direction::Up => PostMessageW(hwnd, WM_KEYUP, *code as usize, 0xC0000000)
      }
    };
  } else {
    println!("trying to send unknown key: {:?}", &key_action.key)
  }

}

pub fn process_message() {
  let mut msg: MSG = unsafe { MaybeUninit::zeroed().assume_init() };
  unsafe { GetMessageW(&mut msg, null_mut(), 0, 0) };
}

pub fn install_hook(
  hook_id: libc::c_int,
  hook_proc: unsafe extern "system" fn(libc::c_int, WPARAM, LPARAM) -> LRESULT,
) -> *mut HHOOK__ {
  unsafe { SetWindowsHookExW(hook_id, Some(hook_proc), 0 as HINSTANCE, 0) }
}

pub unsafe extern "system" fn keybd_hook(
  code: libc::c_int,
  w_param: WPARAM,
  l_param: LPARAM,
) -> LRESULT {
  let vk: i32 = (*(l_param as *const KBDLLHOOKSTRUCT))
      .vkCode
      .try_into()
      .expect("vkCode does not fit in i32");
  // https://docs.microsoft.com/en-us/windows/win32/inputdev/wm-keydown
  // Says that we can find the repeat bit here, however that does not apply to lowlvlkb hook which this is.
  // Because IDE is not capable of following to the definition here it is:
  // STRUCT!{struct KBDLLHOOKSTRUCT {
  //     vkCode: DWORD,
  //     scanCode: DWORD,
  //     flags: DWORD,
  //     time: DWORD,
  //     dwExtraInfo: ULONG_PTR,
  // }}

  // Note this seemingly is only activated when ALT is not pressed, need to handle WM_SYSKEYDOWN then
  // Test that case.
  //let key: Keyboard = vk.into();
  match w_param as u32 {
    code if code == WM_KEYDOWN || code == WM_SYSKEYDOWN => {

        if (l_param & 0xFF00) > 0 {
          println!("keydown: {:#018b}", l_param);
        } else {
          println!("keydown repeat: {:#018b}", l_param);
        }
    }
    code if code == WM_KEYUP || code == WM_SYSKEYUP => {
      println!("keyup");
    }
    _ => {}
  }
  CallNextHookEx(null_mut(), code, w_param, l_param)
}

/*
    let hhkLowLevelKybd: *mut HHOOK__ = windows::install_hook(WH_KEYBOARD_LL, windows::keybd_hook);

    // Keep this app running until we're told to stop
    let mut msg: LPMSG = null_mut();
    unsafe {
        while (GetMessageW(msg, null_mut(), 0, 0) != 0) {    //this while loop keeps the hook
            unsafe {
                DispatchMessageW(msg);
            }
        }
    }
    unsafe { UnhookWindowsHookEx(hhkLowLevelKybd); }
    thread::sleep(Duration::from_secs(10));
 */