use hookmap::device::{Button, ButtonAction, ButtonEvent};
use serde::{Deserialize, Serialize};

#[derive(Debug, Eq, PartialEq, Copy, Clone, Serialize, Deserialize)]
pub enum Direction {
    Up,
    Down,
}

impl From<ButtonAction> for Direction {
    fn from(value: ButtonAction) -> Self {
        match value {
            ButtonAction::Press => Direction::Down,
            ButtonAction::Release => Direction::Up,
        }
    }
}

#[derive(Debug, Eq, PartialEq, Copy, Clone, Serialize, Deserialize)]
pub struct KeyEvent {
    pub key: Key,
    pub action: Direction,
    pub injected: bool,
}

impl TryFrom<ButtonEvent> for KeyEvent {
    type Error = ();

    fn try_from(value: ButtonEvent) -> Result<Self, Self::Error> {
        match Key::try_from(value.target) {
            Ok(key) => Ok(KeyEvent {
                key,
                action: Direction::from(value.action),
                injected: value.injected,
            }),
            Err(_) => Err(()),
        }
    }
}


#[derive(Debug, Eq, PartialEq, Copy, Clone, Serialize, Deserialize, Hash)]
pub enum Key {
    W,
    A,
    S,
    D,
    Q,
    E,
    R,
    Space,
}

impl TryFrom<Button> for Key {
    type Error = ();
    fn try_from(value: Button) -> Result<Self, Self::Error> {
        match value {
            Button::W     => Ok(Key::W),
            Button::A     => Ok(Key::A),
            Button::S     => Ok(Key::S),
            Button::D     => Ok(Key::D),
            Button::Q     => Ok(Key::Q),
            Button::E     => Ok(Key::E),
            Button::R     => Ok(Key::R),
            Button::Space => Ok(Key::Space),
            _             => Err(())
        }
    }
}

impl From<Key> for Button {
    fn from(value: Key) -> Self {
        match value {
            Key::W => Button::W,
            Key::A => Button::A,
            Key::S => Button::S,
            Key::D => Button::D,
            Key::Q => Button::Q,
            Key::E => Button::E,
            Key::R => Button::R,
            Key::Space => Button::Space,
        }
    }
}