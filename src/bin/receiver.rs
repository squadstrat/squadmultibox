#![feature(async_closure)]
#![feature(map_try_insert)]
#![allow(warnings, unused)]

use std::net::{IpAddr, Ipv4Addr, SocketAddr};
use std::{env, thread};
use std::time::Duration;
use message_io::network::{NetEvent, Transport};
use message_io::node::{self, NodeEvent};
use squadmultibox::log::start_log;
use serde_json;
use squadmultibox::keyboard::KeyEvent;
use squadmultibox::windows;

// receiver
fn main() {
    start_log();
    let args: Vec<String> = env::args().collect();
    let (handler, listener) = node::split::<()>();
    //let addr = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)), 8080);
    let addr = args[1].clone();
    handler.network().listen(Transport::FramedTcp, addr);
    let maybe_handle = windows::get_window_handle();
    if let Some(window_handle) = windows::get_window_handle() {
        listener.for_each(move |event| match event {
            NodeEvent::Network(net_event) => match net_event {
                NetEvent::Connected(_endpoint, _ok) => {println!("Network connect - wrong connection")},
                NetEvent::Accepted(_endpoint, _listener) => println!("Client connected"), // Tcp or Ws
                NetEvent::Message(_endpoint, data) => {

                    let key_event: KeyEvent = serde_json::from_slice(data).unwrap();
                    println!("Received Message: {:?}", key_event);
                    windows::send_key(window_handle, key_event);
                },
                NetEvent::Disconnected(_endpoint) => println!("Client disconnected"),
            },
            NodeEvent::Signal(signal) => match signal {
                key => {//
                    println!("Received Signal.", );
                }
            }
        });
    } else {
        println!("window not found, exiting in 5");
        thread::sleep(Duration::from_secs(5));
    }
}