#![feature(async_closure)]
#![feature(map_try_insert)]
#![allow(warnings, unused)]

use std::collections::HashMap;
use std::{env, thread};
use std::net::{IpAddr, Ipv4Addr, SocketAddr};
use std::sync::{Arc, Mutex};
use std::time::Duration;
use message_io::network::{Endpoint, NetEvent, Transport};
use message_io::node::{self, NodeEvent, NodeHandler};
use squadmultibox::log::start_log;
use squadmultibox::windows;
use hookmap::prelude::*;
use squadmultibox::keyboard::{Direction, Key, KeyEvent};
use squadmultibox::windows::Window;


type KeyDownMap = Arc<Mutex<HashMap<Button, bool>>>;

struct ProgramState {
    send_keys: bool,
}

#[macro_use]
fn main() {
    start_log();
    let maybe_handle = windows::get_window_handle();
    let Some(window_handle) = windows::get_window_handle() else {
        println!("Squad window not found, exiting in 5");
        thread::sleep(Duration::from_secs(5));
        return;
    };
    println!("Found Squad window");
    let args: Vec<String> = env::args().collect();
    let (handler, listener) = node::split::<()>();
    //let addr = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)), 8080);
    let addr = args[1].clone();
    let (endpoint, sock_addr) = handler.network().connect(Transport::FramedTcp, addr.clone()).unwrap();
    let debug_handler = handler.clone();

    println!("Connecting to: {}", addr.clone());

    let mut key_down: KeyDownMap = Arc::new(Mutex::new(HashMap::new()));
    let mut program_state = Arc::new(Mutex::new(ProgramState { send_keys: false}));
    let mut hotkey = Hotkey::new();
    hotkey
        .register(Context::new()
            .native_event_operation(NativeEventOperation::Block))
        .on_press(buttons!(W, A, S, D, R, Space),
                  on_captured_key_down(program_state.clone(),handler.clone(), endpoint, key_down.clone()))
        .on_release(buttons!(W, A, S, D, R, Space),
                    on_captured_key_up(program_state.clone(), handler.clone(), endpoint, key_down.clone()))
        .on_press(buttons!(F6), on_control_key_down(program_state.clone()))
    ;
    println!("press F6 to start sending vehicle control keys");
    hotkey.install();
}

fn on_captured_key_down<M>(program_state: Arc<Mutex<ProgramState>>, handler: NodeHandler<M>, endpoint: Endpoint, key_state_map: Arc<Mutex<HashMap<Button, bool>>>) -> impl Fn(ButtonEvent) {
    move |event: ButtonEvent| {
        let mut state = program_state.lock().unwrap();
        if !state.send_keys {
            event.target.press();
            return;
        }
        let mut key_states = key_state_map.lock().unwrap();
        let current_key_down = if let Some(current_key_down) = key_states.get(&event.target) {
            let result = *current_key_down;
            key_states.insert(event.target.clone(), true);
            result
        } else {
            false
        };
        // i know this code hurts
        if !current_key_down {
            let mut key_event = KeyEvent::try_from(event).unwrap();
            if (key_event.key == Key::Q){
                key_event.key = Key::A;
                //windows::send_key(*state.window_handle, key_event);
            } else if (key_event.key == Key::E){
                key_event.key = Key::D;
                //windows::send_key(*state.window_handle, key_event);
            }  else if (key_event.key == Key::R) {
                key_event.key = Key::E // should be a proper mapping on construction...
            }
            handler.network().send(endpoint, serde_json::to_string(&key_event).unwrap().as_bytes());
        }
    }
}

fn on_captured_key_up<M>(program_state: Arc<Mutex<ProgramState>>, handler: NodeHandler<M>, endpoint: Endpoint, key_state_map: Arc<Mutex<HashMap<Button, bool>>>) -> impl Fn(ButtonEvent) {
    move |event: ButtonEvent| {
        //let mut state = program_state.lock().unwrap();
        let mut key_states = key_state_map.lock().unwrap();
        let current_key_down = if let Some(current_key_down) = key_states
            .get(&event.target) { *current_key_down } else { false };
        //println!("{:?}", state.send_keys);
        key_states.insert(event.target.clone(), false);
        let mut key_event = KeyEvent::try_from(event).unwrap();
        if (key_event.key == Key::R) {
            key_event.key = Key::E // should be a proper mapping on construction...
        }
        handler.network().send(endpoint, serde_json::to_string(&key_event).unwrap().as_bytes());
    }
}

fn on_control_key_down(program_state: Arc<Mutex<ProgramState>>) -> impl Fn(ButtonEvent) {
    move |event: ButtonEvent| {
        let mut state = program_state.lock().unwrap();
        state.send_keys = !state.send_keys;
        if state.send_keys {
            println!(">>> >>> >>> Sending ON")
        } else {
            println!("*** *** *** Sending OFF")
        }
    }
}