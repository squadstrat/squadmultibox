#[macro_use(lazy_static)]
extern crate lazy_static;

pub mod log;
pub mod windows;
pub mod keyboard;
pub mod keycodes;