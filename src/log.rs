use simple_log::LogConfigBuilder;

pub fn start_log() -> Result<(), String> {
    let log_level = if cfg!(debug_assertions) {
        "debug"
    } else {
        "error"
    };

    let config = LogConfigBuilder::builder()
        .level(log_level.clone())
        .output_console()
        .build();

    simple_log::new(config)?;
    println!("Log level: {:?}", log_level);
    Ok(())
}